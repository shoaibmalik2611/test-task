import 'package:flutter/material.dart';
import 'package:kostenlos/application/theme/app_colors.dart';


String get latoFont => "Lato";

TextTheme get textTheme =>  TextTheme(
      displayLarge: TextStyle(fontSize: 24, fontWeight: FontWeight.w600, fontFamily: latoFont),
      displayMedium: TextStyle(fontSize: 22, fontWeight: FontWeight.w600, fontFamily: latoFont),
      displaySmall: TextStyle(fontSize: 20, fontWeight: FontWeight.w600, fontFamily: latoFont),
      titleLarge: TextStyle(fontSize: 18, fontWeight: FontWeight.w600, fontFamily: latoFont),
      titleMedium: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, fontFamily: latoFont),
      titleSmall: TextStyle(fontSize: 15, fontWeight: FontWeight.w600, fontFamily: latoFont),
      bodyLarge: TextStyle(fontSize: 14, fontWeight: FontWeight.w600, fontFamily: latoFont),
      bodyMedium: TextStyle(fontSize: 14, fontWeight: FontWeight.w400, fontFamily: latoFont),
      bodySmall: TextStyle(fontSize: 12, fontWeight: FontWeight.w400, fontFamily: latoFont),
      labelLarge: TextStyle(fontSize: 11, fontWeight: FontWeight.w500, fontFamily: latoFont),
      labelMedium: TextStyle(fontSize: 9, fontWeight: FontWeight.w400, fontFamily: latoFont),
    ).apply(displayColor: AppColor.dark, bodyColor: AppColor.dark);
